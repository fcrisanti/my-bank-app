package pl.bank.app.domain.user

import pl.bank.app.domain.user.exceptions.LoginAlreadyExistsException
import spock.lang.Specification

class CreateUserWithAccountTest extends Specification {

    CreateUserWithAccount createUserWithAccount = Mock(CreateUserWithAccount)
    CreateUserCommand createUserCommand = Mock(CreateUserCommand)
    UserRetrievalClient userRetrievalClient = Mock(UserRetrievalClient)

    def "creation of a user with existing login"() {
        //testujemy próbę utworzenia dwóch użytkowników o tym samym loginie
        given:

        String login = "test"
        createUserCommand.getLogin() >> login
        userRetrievalClient.findByLogin(login).isPresent() >> true //funkcja wywoływana przez ifLoginExistsThrow(String login)

        when:
//        createUserWithAccount.createDefaultAccount(createUserCommand)
        userRetrievalClient.findByLogin(login).isPresent() //powinienem testować funkcję zakomentowaną wyżej ale znalazłem, że konkretnie ten fragment wywołuje NullPointera mimo, że przypisałem na twardo wartość wyżej

        then:
        thrown(LoginAlreadyExistsException)
    }
}
