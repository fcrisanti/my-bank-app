package pl.bank.app.domain.transfer
import pl.bank.app.domain.account.Account
import pl.bank.app.domain.transfer.exceptions.UserNotFound
import pl.bank.app.domain.user.AccountRetrievalClient
import pl.bank.app.domain.user.CreateTransferClient
import pl.bank.app.domain.user.CreateTransferCommand
import pl.bank.app.domain.user.CreateUserCommand
import pl.bank.app.domain.user.User
import spock.lang.Specification


class TransferExecutorTest extends Specification {

    CreateTransferCommand createTransferCommand = Mock(CreateTransferCommand);
    CreateUserCommand createUserCommand = Mock(CreateUserCommand);
    TransferExecutor transferExecutor = Mock(TransferExecutor)

    @Shared
    Account account = Account.generateAccount()
    CreateUserCommand command = new CreateUserCommand("Francesco", "Crisanti", "Franek")
    User user = User.generateUser(command)

    def "checking whether user has an account"() {
        given:

        Account account = Account.generateAccount()
        User user = User.generateUser(createUserCommand)
        // user.addAccount(account)
        // zakomentowałem wyżej^ dodanie konta do użytkownika więc powinien być błąd.
        // Gdy usuniesz komentarz user.isAccount(account) będzie true

        when:
        transferExecutor.userHasAccountOrThrow(user,account)

        then:
        notThrown(UserNotFound)
    }
}
