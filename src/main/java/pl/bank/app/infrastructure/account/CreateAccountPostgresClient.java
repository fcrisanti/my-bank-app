package pl.bank.app.infrastructure.account;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.bank.app.domain.account.Account;
import pl.bank.app.domain.user.CreateAccountClient;

@Service
@RequiredArgsConstructor
class CreateAccountPostgresClient implements CreateAccountClient {
    private final AccountRepository accountRepository;

    @Override
    public void create(Account account) {
        accountRepository.save(account);
    }
}
