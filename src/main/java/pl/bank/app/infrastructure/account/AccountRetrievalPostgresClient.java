package pl.bank.app.infrastructure.account;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.bank.app.domain.account.Account;
import pl.bank.app.domain.user.AccountRetrievalClient;

@Service
@RequiredArgsConstructor
class AccountRetrievalPostgresClient implements AccountRetrievalClient {

    public final AccountRepository accountRepository;

    @Override
    public Account getById(Long accountId) {
       return accountRepository.getOne(accountId);
    }
}
