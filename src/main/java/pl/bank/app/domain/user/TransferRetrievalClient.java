package pl.bank.app.domain.user;

import java.util.List;

public interface TransferRetrievalClient {

    List<CreateTransferCommand> getTransferRequests();

}
