package pl.bank.app.domain.user;

import pl.bank.app.domain.account.Account;

public interface CreateAccountClient {
    void create(Account account);
}
