package pl.bank.app.domain.user;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserFacade {

    private final CreateUserWithAccount createUserWithAccount;

        public void createUserWithNewAccount(CreateUserCommand createUserCommand) {
        createUserWithAccount.createDefaultAccount(createUserCommand);
    }

}
