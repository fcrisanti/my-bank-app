package pl.bank.app.domain.user;

import lombok.Builder;
import lombok.Getter;
import org.springframework.lang.NonNull;
import pl.bank.app.api.transfer.TransferRequest;

import java.math.BigDecimal;

@Builder
@Getter
public class CreateTransferCommandMapper {

    public static CreateTransferCommand map(TransferRequest transferRequest) {
        return CreateTransferCommand.builder()
                .title(transferRequest.getTitle())
                .auctionOwnerId(transferRequest.getAuctionOwnerId())
                .auctionOwnerAccountId(transferRequest.getAuctionOwnerAccountId())
                .auctionWinnerAccountNumber(transferRequest.getAuctionWinnerAccountNumber())
                .amount(transferRequest.getAmount())
                .build();
    }
}
