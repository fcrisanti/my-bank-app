package pl.bank.app.domain.user;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AccountFacade {

    public static String generateNewAccountNumber() {
        return AccountNumberGenerator.generate();
    }

}
