package pl.bank.app.domain.user;

class AccountNumberGenerator {

    private static final String NUMERIC_STRING = "0123456789";
    private static final int ACCOUNT_NUMBER_LENGTH = 12;

    public static String generate() {
        StringBuilder builder = new StringBuilder();
        for (int i=ACCOUNT_NUMBER_LENGTH; i>0; i--)
        {
            int character = (int) (Math.random() * NUMERIC_STRING.length());
            builder.append(NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

}
