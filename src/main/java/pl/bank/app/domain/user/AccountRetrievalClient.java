package pl.bank.app.domain.user;

import pl.bank.app.domain.account.Account;

public interface AccountRetrievalClient {

    public Account getById(Long accountId);
}
