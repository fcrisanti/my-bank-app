package pl.bank.app.domain.account;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.bank.app.domain.transfer.exceptions.NotEnoughAmount;
import pl.bank.app.domain.user.AccountFacade;
import pl.bank.app.domain.user.User;
import pl.bank.app.shared.Auditable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import java.math.BigDecimal;

@Entity
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Getter
@Setter(AccessLevel.PRIVATE)
public class Account extends Auditable {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "account_sequence")
    @SequenceGenerator(name = "account_sequence")
    private Long id;

    private String accountNumber;

    private BigDecimal amount;

    private final BigDecimal INITIAL_AMOUNT = BigDecimal.valueOf(1000); //FOR TESTING PURPOSE

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "users")
    @Setter
    private User user;

    public static Account generateAccount() {
        Account account = new Account();
        account.setAmount(account.INITIAL_AMOUNT);
        account.setAccountNumber(AccountFacade.generateNewAccountNumber());
        return account;
    }

//    public static Account generateEmptyAccount() {
//        Account account = new Account();
//        account.setAmount(BigDecimal.ZERO);
//        account.setAccountNumber(AccountFacade.generateNewAccountNumber());
//        return account;
//    }

    public boolean isEnoughMoneyToPay(BigDecimal payment) {
        return (amount.subtract(payment).compareTo(BigDecimal.ZERO) >= 0);
    }

    public void subtractAmount(BigDecimal payment) {
        accountHasAmountOrThrow(payment);
        this.amount = amount.subtract(payment);
    }

    public void accountHasAmountOrThrow(BigDecimal amount) throws NotEnoughAmount {
        if (!this.isEnoughMoneyToPay(amount))
            throw new NotEnoughAmount(String.format("Account %d does not have enough money. Has %d, should be >= %d", this.getId(), this.getAmount().intValue(), amount.intValue()));
    }
}
